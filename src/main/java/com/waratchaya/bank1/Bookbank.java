/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.bank1;

/**
 *
 * @author Melon
 */
public class Bookbank {
    private String name;
    private double balance;
    
    public Bookbank(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }
    
    public boolean deposit(double money) {
        if(money <=0 ) {
            System.out.println("Money > 0!!!");
            return false;
        }
        this.balance = this.balance+money;
        System.out.println( "Name: "+this.name+" "+"Money: "+this.balance+" Baht.");
        return true;
    }
    
    public boolean deposit() {
        return this.deposit(this.balance);
    }
    
    public boolean widthraw(double money) {
        if(money<=0) {
            System.out.println("Money > 0!!!");
            return false;
        }
        if(money>this.balance) {
            System.out.println("Not enough money");
            return false;
        }
        this.balance = this.balance-money;
        System.out.println( "Name: "+this.name+" "+"Money: "+this.balance+" Baht.");
        return true;
    }
    
    
@Override
public String toString() {
    return "Name: "+this.name+" "+"Money: "+this.balance+" Baht."; 
}
}
